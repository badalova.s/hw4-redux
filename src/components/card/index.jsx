import { useState } from "react";
import PropTypes from 'prop-types';
import { FaStar } from "react-icons/fa"
import "./card.scss"



export default function Card({image,name,price,color,article}) {

        return (
        
          
          
            <div className="card" >
              
            <div className="card__img"><img style={{
            width: "200px",
            height: "130px"
            }} src={image}></img></div>
            <p className="card__name">{name}</p>
            <p className="card__price">Ціна: {price}</p>
            <p className="card__color">Колір: {color}</p>
            <p className="card__aticle">Артикуль: {article}</p>
            
            </div>
            
            
        )
          }
      
    

    Card.propTypes = {
      name: PropTypes.string,
      color: PropTypes.string,
      price:PropTypes.string,
      article:PropTypes.string,
      Click:PropTypes.func
    }
