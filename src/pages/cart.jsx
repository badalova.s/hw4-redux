
import Card from "../components/card"
import Header from "../header";
import { useEffect, useState } from "react";
import { FaTrash } from "react-icons/fa";
import "./cart.scss"
import Modal from "../components/modal";
import {useDispatch} from "react-redux";
import { toggleModal } from "../redux/actions/modal";
import { connect } from 'react-redux';
import { deleteCart } from "../redux/actions/card";

function Cart ({cart,deleteCart}){
    let [its,setIt]=useState(null)
    const dispatch = useDispatch();

    if (cart!=null){
    return (
        <>

    <div className="cart">
    { cart.map(el=> 
      
       <div className="cart__card" key={Math.random()}>
        <Card key={Math.random()}  name={el.name} image={el.image}  
        price={el.price}
        color={el.color}
        article={el.article}/>
        <div className="cart__fatrash"><FaTrash onClick={()=>{dispatch(toggleModal())
        setIt(el.id)

}

} /></div>

</div>
)}

</div>

<Modal
          question={"Ви хочете видалити товар з корзини"}       
          add={()=>{
            dispatch(toggleModal())
            deleteCart(its)
            }}
           />
</>
)
}
}
const mapDispatchToProps = dispatch => ({
    deleteCart: id => dispatch(deleteCart(id))
  });
  export default connect(null, mapDispatchToProps)(Cart)