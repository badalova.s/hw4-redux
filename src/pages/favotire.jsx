import Header from "../header";
import { useEffect, useState } from "react";
import Card from "../components/card"
import { FaTrash } from "react-icons/fa";
import "./favorite.scss"
import Modal from "../components/modal";
import {useDispatch} from "react-redux";
import { toggleModal } from "../redux/actions/modal";
import { deleteFavorite } from "../redux/actions/favorite";
 import { connect } from "react-redux";
function Favorite ({favorite,deleteFavorite}){
    let [its,setIt]=useState(null)
    const dispatch = useDispatch();
        
    if (favorite!=null){
    return (
        <>

    <div className="favorite">
    { favorite.map(el=> 
      
       <div className="favorite__card" key={Math.random()}>
<Card key={Math.random()}  name={el.name} image={el.image} 
price={el.price}
color={el.color}
article={el.article}/>
<div className="cart__fatrash">
<FaTrash onClick={()=>{{dispatch(toggleModal())

setIt(el.id)}

}

} /></div>

</div>
)}

</div>
<Modal
question={"Ви хочете видалити товар з обраного"} 
add={()=>{dispatch(toggleModal())
deleteFavorite(its)
}}
           />
</>)}
}
const mapDispatchToProps = dispatch => ({
    deleteFavorite: id => dispatch(deleteFavorite(id))
  });
  export default connect(null, mapDispatchToProps)(Favorite)