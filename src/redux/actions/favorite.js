export const addFavorite = item => ({
    type: 'ADD_FAVORITE',
    payload: item
  });
  export const deleteFavorite = id => ({
    type: 'DELETE_FAVORITE',
    payload: id
  });
  
  export const addItemToFavorite = (favoriteItems, favoriteItemToAdd) => {
    if(favoriteItems!==undefined){

    if (favoriteItems.find(
        favoriteItems => favoriteItems.id === favoriteItemToAdd.id
    )) {
        alert("Цей товар вже додано до обранного")
      return favoriteItems.map(favoriteItems => 
        favoriteItems.id === favoriteItemToAdd.id
          ? { ...favoriteItems}
          : favoriteItems
      );
    }
  
    return [...favoriteItems, { ...favoriteItemToAdd }];}
    else
    return [favoriteItemToAdd]
}



export const deleteItemFavorite = (favoriteItems, id) => favoriteItems.filter(item => item.id !== id);