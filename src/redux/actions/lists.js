export function fillLists(data) {
    return {
      type: 'FILL_LISTS',
      payload: data,
    };
  }
  
  export function getListssAsync() {
    return async function (dispatch) {
      const data = await fetch("./list.json").then((res) =>
        res.json()
      );
      dispatch(fillLists(data));
    };
  }
  