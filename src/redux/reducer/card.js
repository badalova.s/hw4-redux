import { addItemToCart, deleteItemCart } from "../actions/card";

const initialState = {
  cartItems:[]
}

export function cardsReducer  (state = initialState, action)  {
  switch (action.type) {
    case 'ADD_CART':
      return {
        
        cartItems: addItemToCart(state.cartItems, action.payload)
        
      }
      case 'DELETE_CART':
  return {
    ...state,
    cartItems: deleteItemCart(state.cartItems, action.payload)
  }
      
    default:
      return state;
  }
}


