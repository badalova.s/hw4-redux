const initialState = {
    modal: false

}

export function modalReducer(state = initialState, action) {
    switch (action.type) {
        case 'TOGGLE_MODAL':
            return {
                ...state,
                modal: state.modal===false?true:false
            }
            default:
                return state

        }
}
