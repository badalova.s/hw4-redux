import { combineReducers } from "redux";

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 

// Reducers

import {modalReducer as modal} from "./reducer/modal";
import { listsReducer as lists } from "./reducer/lists";
import {cardsReducer as carts} from "./reducer/card";
import { favoriteReducer as favorite } from "./reducer/favorite";

export const rootReducer = combineReducers({modal,lists,carts,favorite});
