
import { createStore, applyMiddleware } from 'redux'

import { rootReducer } from './roodReducer';

import {
    composeEnhancers,
    middleware,
} from './middleware';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web



const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['carts','favorite'] 
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(
   persistedReducer,
    composeEnhancers(applyMiddleware(...middleware))
);

export const persistor=persistStore(store)